package fr.cnam.foad.nfa035.badges.wallet.dao.impl;

import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.impl.WalletFrame;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.ImageDeserializerBase64DatabaseImpl;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.util.List;

public class DirectAccessBadgeWalletDAOImpl implements DirectAccessBadgeWalletDAO {
    private final File walletDatabase;

    public DirectAccessBadgeWalletDAOImpl(String dbPath) throws IOException {
        this.walletDatabase = new File(dbPath);
    }

    /**
     * permet de mettre à jour l'amorce à chaque ajout de badge au Wallet
     * @param image
     * @throws Exception
     */
    @Override
    public void addBadge(File image) throws Exception {
        try (WalletFrame media = new WalletFrame(new RandomAccessFile(walletDatabase, "rw"))) {
            // A COMPLéTER
        }
    }

    @Override
    public void getBadge(OutputStream imageStream) throws Exception {
        try (WalletFrame media = new WalletFrame(new RandomAccessFile(walletDatabase, "r"))) {
            new ImageDeserializerBase64DatabaseImpl(imageStream).deserialize(media);
        }
    }

    /**
     * permet de récupérer l'ensemble des métadonnées du Wallet par balayage en flux de toutes les lignes du fichier csv
     * @return
     * @throws IOException
     */
    @Override
    public List<DigitalBadgeMetadata> getWalletMetadata() throws Exception {
        try (WalletFrame media = new WalletFrame(new RandomAccessFile(walletDatabase, "r"))) {
            // A COMPLéTER
        }
        return null; // pas bon
    }

    /**
     * permet de récupérer un seule et unique Badge par accès direct
     * @param imageStream
     * @param meta
     * @throws IOException
     */
    @Override
    public void getBadgeFromMetadata(OutputStream imageStream, DigitalBadgeMetadata meta) throws Exception {
        List<DigitalBadgeMetadata> metas = this.getWalletMetadata();
        try (WalletFrame media = new WalletFrame(new RandomAccessFile(walletDatabase, "rws"))) {
            // A COMPLéTER
        }
    }
}

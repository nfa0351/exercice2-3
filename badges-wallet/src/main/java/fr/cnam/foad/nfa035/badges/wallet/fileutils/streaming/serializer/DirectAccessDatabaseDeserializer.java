package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ImageFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.WalletFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;

import java.io.IOException;
import java.io.OutputStream;

public interface DirectAccessDatabaseDeserializer<M extends ImageFrameMedia> extends DatabaseDeserializer<M> {

    /**
     * Méthode principale de désérialisation, consistant au transfert simple du flux de lecture vers un flux d'écriture
     * @param media
     * @throws IOException
     */
    //@Override car n'est pas dans DatabaseDeserializer
    void deserialize(WalletFrameMedia media, DigitalBadgeMetadata meta) throws IOException;

    /**
     * Utile pour récupérer un Flux d'écriture sur la source à restituer
     * @return
     * @param <T>
     */
    @Override
    <T extends OutputStream> T getSourceOutputStream();

    /**
     * Utile pour modifier le flux de sortie au format source
     * @param os
     * @param <T>
     */
    @Override
    <T extends OutputStream> void setSourceOutputStream(T os);
}

package fr.cnam.foad.nfa035.badges.wallet.model;

import java.io.File;

public class DigitalBadge {

    private File badge;

    /**
     * aggregation avec la classe DigitalBadgeMetadata
     */
    private DigitalBadgeMetadata metadata;

    /**
     * constructeur avec 1 paramètre
     * @param badge
     */
    public DigitalBadge(File badge) {
        this.badge = badge;
        this.metadata = new DigitalBadgeMetadata(badge);
    }

    public void equals() {
        // TODO: 25/11/2022  corps ??
    }

    public DigitalBadgeMetadata getMetadata() {
        return metadata;
    }
    
    public void setMetadata(DigitalBadgeMetadata metadata) {
        this.metadata = metadata;
    }

    public File getBadge() {
        return badge;
    }
    
    public int hashCode() {
        // TODO: 25/11/2022 corps ?? 
        return 0;
    }

    public void setBadge(File badge) {
        this.badge = badge;
    }

    public String toString() {
        return "badge" + badge + "metadata" + metadata;
    }


}

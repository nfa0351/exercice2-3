package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.WalletFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;

import java.io.IOException;
import java.util.List;

/**
 * pour la désérialisation des métadonnées
 */
public interface MetadataDeserializer {
    List<DigitalBadgeMetadata> deserialize(WalletFrameMedia media) throws IOException;
}

package fr.cnam.foad.nfa035.badges.wallet.model;

import java.io.File;

public class DigitalBadgeMetadata {

    /**
     * constructeur par défaut
     */
    public DigitalBadgeMetadata() {
    }

    /**
     * contructeur avec les 3 paramètres
     * @param badgeid
     * @param imageSize
     * @param walletPosition
     */
    public DigitalBadgeMetadata(int badgeid, long imageSize, long walletPosition) {
        this.badgeid = badgeid;
        this.imageSize = imageSize;
        this.walletPosition = walletPosition;
    }

    /**
     * constructeur par défaut
     * @param badge
     */
    public DigitalBadgeMetadata(File badge) {
    }

    private int badgeid;

    public int getBadgeid() {
        return badgeid;
    }

    public void setBadgeid(int badgeid) {
        this.badgeid = badgeid;
    }

    public long getImageSize() {
        return imageSize;
    }

    public void setImageSize(long imageSize) {
        this.imageSize = imageSize;
    }

    private long imageSize;

    private long walletPosition;

    public void equals() {
        // TODO: 25/11/2022  corps ??
    }

    public long getWalletPosition() {
        return walletPosition;
    }

    public int hashCode() {
        // TODO: 25/11/2022 corps ??
        return 0;
    }

    public void setWalletPosition(long walletPosition) {
        this.walletPosition = walletPosition;
    }

    public String toString() {
        return "badgeid" + badgeid + "imagesize" + imageSize + "walletPosition" + walletPosition;
    }

}
